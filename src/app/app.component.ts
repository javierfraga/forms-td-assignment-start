import { Component,ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	@ViewChild('f') form:NgForm;
	private submitted:boolean = false;
	private option = 'advanced';
	private options = [
		{
			value: 'basic',
			display: 'Basic',
		},
		{
			value: 'advanced',
			display: 'Advanced',
		},
		{
			value: 'pro',
			display: 'Pro',
		},
	]

	onSubmit() {
		this.submitted = true;
	}

	reset() {
		this.form.reset();
		this.submitted = false;
	}
}
